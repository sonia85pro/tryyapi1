import axios from 'axios'

export default class Testy {
  static affOpt() {
    return axios.get('http://localhost:3000/users/').catch(function (error) {
      console.log(error);
    });
  }
  static addOpt(name, forename, city, zipcode, password) {
    return axios.post("http://localhost:3000/users", {
      name: name,
      forename: forename,
      city: city,
      zipcode: zipcode,
      password: password

    });
  }
  static removeOpt(id) {
    return axios.delete("http://localhost:3000/users/" + id).catch(function (error) {
      console.log(error);
    });
  }
  static getTotal() {
    return axios.get('http://localhost:3000/users/').catch(function (error) {
      console.log(error);
    });
  }

  static getOptById(operationId) {
    return axios.get("http://localhost:3000/users/" + operationId)
  }

}